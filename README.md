# gunicornapp-by-docker
General template for launching wsgi application with gunicorn container.

## Requirements
- docker
- docker-compose
## How to launch
`docker-compose up -d`
## How to assign specific parameters
### For gunicorn settings
It can be used `gunicorn.py` as settings for gunicorn in current path.  
How to make settings should be reffered to official [link](https://docs.gunicorn.org/en/stable/settings.html).  
`wsgi_app` and `chdir` properties should be used for self path construction.

### env_file for additional environment variables for app
It can be used `.env.local` in current path if it is necessary for app.

### Required to put .env for properties based on .env.default
Two properties `APP_PORT` and `ROOT_PATH` are required to build container.

## Actual command by docker-compose
Command to launch gunicorn by docker is  
`gunicorn -b 0.0.0.0:5000 --config gunicorn.py`

## Requirements for app
`wsgi-app/requirements.txt` is used as requirement package list for app.  
They are installed by pip before launching app by gunicorn.

## Layout of directory
```
- current_dir/
    wsgi-app/
        requirements.txt
        ...
    gunicorn.py
    .env.local
    .env
```
