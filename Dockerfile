FROM python:3.7-alpine
ENV PYTHONUNBUFFERED 1

RUN apk add --update git

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ADD . /usr/src/app

ARG ROOT_PATH

RUN pip install -r $ROOT_PATH/requirements.txt
RUN pip install gunicorn

CMD ["gunicorn", "-b", "0.0.0.0:5000", "--config", "gunicorn.py"]
